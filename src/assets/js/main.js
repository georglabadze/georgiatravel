var mapStyle = [
	{
		"featureType": "administrative",
		"elementType": "all",
		"stylers":     [
			{
				"visibility": "on"
			},
			{
				"lightness": 33
			}
		]
	},
	{
		"featureType": "administrative",
		"elementType": "labels",
		"stylers":     [
			{
				"saturation": "-100"
			}
		]
	},
	{
		"featureType": "administrative",
		"elementType": "labels.text",
		"stylers":     [
			{
				"gamma": "0.75"
			}
		]
	},
	{
		"featureType": "administrative.neighborhood",
		"elementType": "labels.text.fill",
		"stylers":     [
			{
				"lightness": "-37"
			}
		]
	},
	{
		"featureType": "landscape",
		"elementType": "geometry",
		"stylers":     [
			{
				"color": "#f9f9f9"
			}
		]
	},
	{
		"featureType": "landscape.man_made",
		"elementType": "geometry",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"lightness": "40"
			},
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "landscape.natural",
		"elementType": "labels.text.fill",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"lightness": "-37"
			}
		]
	},
	{
		"featureType": "landscape.natural",
		"elementType": "labels.text.stroke",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"lightness": "100"
			},
			{
				"weight": "2"
			}
		]
	},
	{
		"featureType": "landscape.natural",
		"elementType": "labels.icon",
		"stylers":     [
			{
				"saturation": "-100"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "all",
		"stylers":     [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "geometry",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"lightness": "80"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "labels",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"lightness": "0"
			}
		]
	},
	{
		"featureType": "poi.attraction",
		"elementType": "geometry",
		"stylers":     [
			{
				"lightness": "-4"
			},
			{
				"saturation": "-100"
			}
		]
	},
	{
		"featureType": "poi.park",
		"elementType": "geometry",
		"stylers":     [
			{
				"color": "#c5dac6"
			},
			{
				"visibility": "on"
			},
			{
				"saturation": "-95"
			},
			{
				"lightness": "62"
			}
		]
	},
	{
		"featureType": "poi.park",
		"elementType": "labels",
		"stylers":     [
			{
				"visibility": "on"
			},
			{
				"lightness": 20
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "all",
		"stylers":     [
			{
				"lightness": 20
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"gamma": "1.00"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels.text",
		"stylers":     [
			{
				"gamma": "0.50"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels.icon",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"gamma": "0.50"
			},
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "road.highway",
		"elementType": "geometry",
		"stylers":     [
			{
				"color": "#c5c6c6"
			},
			{
				"saturation": "-100"
			}
		]
	},
	{
		"featureType": "road.highway",
		"elementType": "geometry.stroke",
		"stylers":     [
			{
				"lightness": "-13"
			}
		]
	},
	{
		"featureType": "road.highway",
		"elementType": "labels.icon",
		"stylers":     [
			{
				"lightness": "0"
			},
			{
				"gamma": "1.09"
			}
		]
	},
	{
		"featureType": "road.arterial",
		"elementType": "geometry",
		"stylers":     [
			{
				"color": "#e4d7c6"
			},
			{
				"saturation": "-100"
			},
			{
				"lightness": "47"
			}
		]
	},
	{
		"featureType": "road.arterial",
		"elementType": "geometry.stroke",
		"stylers":     [
			{
				"lightness": "-12"
			}
		]
	},
	{
		"featureType": "road.arterial",
		"elementType": "labels.icon",
		"stylers":     [
			{
				"saturation": "-100"
			}
		]
	},
	{
		"featureType": "road.local",
		"elementType": "geometry",
		"stylers":     [
			{
				"color": "#fbfaf7"
			},
			{
				"lightness": "77"
			}
		]
	},
	{
		"featureType": "road.local",
		"elementType": "geometry.fill",
		"stylers":     [
			{
				"lightness": "-5"
			},
			{
				"saturation": "-100"
			}
		]
	},
	{
		"featureType": "road.local",
		"elementType": "geometry.stroke",
		"stylers":     [
			{
				"saturation": "-100"
			},
			{
				"lightness": "-15"
			}
		]
	},
	{
		"featureType": "transit.station.airport",
		"elementType": "geometry",
		"stylers":     [
			{
				"lightness": "47"
			},
			{
				"saturation": "-100"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "all",
		"stylers":     [
			{
				"visibility": "on"
			},
			{
				"color": "#acbcc9"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "geometry",
		"stylers":     [
			{
				"saturation": "53"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "labels.text.fill",
		"stylers":     [
			{
				"lightness": "-42"
			},
			{
				"saturation": "17"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "labels.text.stroke",
		"stylers":     [
			{
				"lightness": "61"
			}
		]
	}
];
var mapPinAlignment = function (elem) {
	var width = $(elem).innerWidth();
	var windowWidth = $(window).innerWidth();
	if (width <= 690 || width == windowWidth) {
		return 0;
	} else {
		return 1;
	}
}
var gMap = function (selector, callback) {
	this.element = selector;
	this.params = {};
	this.callback = callback;

	this.init = function () {
		this.obtainParams();
		this.callback(this.element, this.params.lat, this.params.lng);
	}

	this.obtainParams = function () {
		this.params.lat = $(this.element).attr('data-lat');
		this.params.lng = $(this.element).attr('data-lng');
	}
	this.init();
}

function initMap () {
	$('.map').gMap(function (element, lat, lng) {
		var lat = Number(lat);
		var lng = Number(lng);
		var position = {lat: lat, lng: lng};
		var map = new google.maps.Map(element, {
			zoom:             8,
			center:           position,
			disableDefaultUI: true,
			styles:           mapStyle,
		});
		var marker = new google.maps.Marker({
			position: {lat: lat, lng: lng},
			map:      map,
			icon:     'assets/img/map-pin.png'
		});
		map.setCenter(new google.maps.LatLng(lat, lng - mapPinAlignment($('.map'))));

		$('body').on('click', '.viewBtn', function (ev) {
			ev.preventDefault();
			var lat = JSON.parse($(this).attr('cor-lat'));
			var lng = JSON.parse($(this).attr('cor-lng'));

			if (marker != null) {
				marker.setMap(null);
				marker = null;
			}
			marker = new google.maps.Marker({
				position: {lat: lat, lng: lng},
				map:      map,
				icon:     'assets/img/map-pin.png'
			})
			map.setCenter(marker.getPosition())
		})
	});
}

function extendMaps () {
	$.fn.extend({
		gMap: function (callback) {
			$(this).each(function (index, item) {
				var gmp = new gMap(item, callback);
			});
		}
	});
}

function setBasicCarousel (elem, param) { //Owl Carousel properties
	var owl = elem.owlCarousel({
		loop:               param.loop,
		margin:             param.margin,
		rewind:             param.rewind,
		nav:                param.nav,
		touchDrag:          param.touchDrag,
		dots:               param.dots,
		animateIn:          param.animateIn,
		animateOut:         param.animateOut,
		mouseDrag:          param.mouseDrag,
		autoplay:           param.autoplay,
		autoplaySpeed:      param.autoplaySpeed,
		autoplayTimeout:    param.autoplayTimeout,
		autoplayHoverPause: param.autoplayHoverPause,
		responsive:         param.responsive,
		onInitialized:      param.onInitialized,
		onTranslate:        param.onTranslate,
		onTranslated:       param.onTranslated,
		navContainer:       param.navContainer,
		dotsContainer:      param.dotsContainer,
	});
	if (param.lArrow) {
		param.lArrow.click(function (e) {
			e.preventDefault();
			owl.trigger('prev.owl.carousel', [300]);
		});
	}
	if (param.rArrow) {
		param.rArrow.click(function (e) {
			e.preventDefault();
			owl.trigger('next.owl.carousel');
		});
	}
	return owl;
}

function setCarouselWithProgressBar (selector, options) {
	let element = $(selector);
	options = options || {};
	let defaultOptions = {
		lArrow:             options.lArrow,
		rArrow:             options.rArrow,
		margin:             options.margin,
		autoplayHoverPause: options.autoplayHoverPause,
		autoplay:           true,
		autoplayTimeout:    10000,
		nav:                true,
		loop:               false,
		rewind:             true,
		dots:               true,
		onInitialized:      startProgressBar,
		onTranslate:        resetProgressBar,
		onTranslated:       startProgressBar,
		navContainer:       `.navContainer[data-target="${selector}"]`,
		dotsContainer:      `.dotsContainer[data-target="${selector}"]`,
		responsive:         {
			0: {
				items: 1
			},
		},
		progressBarElement: $(`.progress-bar[data-target="${selector}"]`)
	}
	for (let i in options) {
		if (defaultOptions.hasOwnProperty(i)) {
			defaultOptions[i] = options[i];
		}
	}

	function startProgressBar () {
		// apply keyframe animation
		defaultOptions.progressBarElement.css({
			width:      "100%",
			transition: `width ${defaultOptions.autoplayTimeout}ms`
		});
	}

	function resetProgressBar () {
		defaultOptions.progressBarElement.css({
			width:      0,
			transition: "width 0s"
		});
	}

	//------- Main Carousel -------//
	setBasicCarousel(element, defaultOptions);
}

function setCarousels () {
	//sliders without progressbar
	setBasicCarousel($('#placesAroundSlider'), {
		rArrow:     $('[data-target="#placesAroundSlider"]'),
		dots:       false,
		loop:       true,
		responsive: {
			0:    {
				items: 1.3
			},
			502:  {
				items: 2
			},
			900:  {
				items: 3
			},
			1000: {
				items: 4
			},
			1200: {
				items: 5
			},
		}
	});

	setBasicCarousel($('#placesToStaySlider'), {
		rArrow:     $('[data-target="#placesToStaySlider"]'),
		dots:       false,
		loop:       true,
		responsive: {
			0:    {
				items: 1.3
			},
			502:  {
				items: 2
			},
			900:  {
				items: 3
			},
			1000: {
				items: 4
			},
			1200: {
				items: 5
			},
		}
	});

	setBasicCarousel($('#reviewSlider'), {
		rArrow:     $('[data-target="#reviewSlider"]'),
		dots:       false,
		loop:       true,
		margin:     30,
		responsive: {
			0:    {
				items: 1.3
			},
			900:  {
				items: 2
			},
			1000: {
				items: 3
			},
			1200: {
				items: 4
			},
		}
	});
	//sliders without progressbar ends

	setCarouselWithProgressBar('#mainCarousel', {
		lArrow:       $('[data-target="#mainCarousel"] .lArrow'),
		rArrow:       $('[data-target="#mainCarousel"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#storiesCarousel', {
		lArrow:       $('[data-target="#storiesCarousel"] .lArrow'),
		rArrow:       $('[data-target="#storiesCarousel"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#popularSlider1', {
		lArrow:       $('[data-target="#popularSlider1"] .lArrow'),
		rArrow:       $('[data-target="#popularSlider1"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#popularSlider2', {
		lArrow:       $('[data-target="#popularSlider2"] .lArrow'),
		rArrow:       $('[data-target="#popularSlider2"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#popularSlider3', {
		lArrow:       $('[data-target="#popularSlider3"] .lArrow'),
		rArrow:       $('[data-target="#popularSlider3"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#popularSlider4', {
		lArrow:       $('[data-target="#popularSlider4"] .lArrow'),
		rArrow:       $('[data-target="#popularSlider4"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#placesSlider', {
		lArrow:       $('[data-target="#placesSlider"] .lArrow'),
		rArrow:       $('[data-target="#placesSlider"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#storiesSlider', {
		lArrow:       $('[data-target="#storiesSlider"] .lArrow'),
		rArrow:       $('[data-target="#storiesSlider"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#aboutSlider', {
		lArrow:       $('[data-target="#aboutSlider"] .lArrow'),
		rArrow:       $('[data-target="#aboutSlider"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#aboutBottSlider', {
		lArrow:       $('[data-target="#aboutBottSlider"] .lArrow'),
		rArrow:       $('[data-target="#aboutBottSlider"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
	});
	setCarouselWithProgressBar('#aboutTopSlider', {
		lArrow:       $('[data-target="#aboutTopSlider"] .lArrow'),
		rArrow:       $('[data-target="#aboutTopSlider"] .rArrow'),
		nav:          false,
		navContainer: false,
		autoplayHoverPause: true,
		margin:       3,
	});
}

function placesImgGallery(){
	$('.places-bg img').eq(0).show();
	$('#placesSlider').on('changed.owl.carousel', function (event) {
		var currentItem = event.item.index;
		$('.places-bg img').hide();
		$('.places-bg img').eq(currentItem).fadeIn();
	})
}

//======= dropdown =======//
function sortDropdown () {
	$('.sort_placeholder').click(function (e) {
		e.stopPropagation();
		var selector = $(this).closest('.sortBtn');
		if (selector.hasClass('open')) {
			selector.removeClass('open');
		} else {
			$('.sort_placeholder').each(function (index, item) {
				$(item).closest('.sortBtn').removeClass('open');
			});
			selector.addClass('open');
		}
	});
	$('.sort_list li').click(function () {
		$(this).closest('.sortBtn').not('[data-target="link"]').find('.sort_placeholder').text($(this).text());
		$(this).closest('.sortBtn').not('[data-target="link"]').find('.sort_placeholder').append('<i class="fa fa-chevron-down"></i>');
		$(this).closest('.sortBtn').removeClass('open');
	});

	$('body', 'html').click(function () {
		$('.sortBtn').removeClass('open');
	})
}

//======= dropdown ends =======//

//======= hamburger button =======//
function hamburgerMenu () {
	$('.hamburger').click(function (e) {
		e.stopPropagation();
		$('.navbar').not('._resp').toggleClass('active');
	});
	$('.navbar').not('._resp').click(function (e) {
		e.stopPropagation();
	});
	$('body', 'html').on('click', function () {
		$('.navbar').not('._resp').removeClass('active');
	})
}


function navbarScroll () {
	var scrollTo = $('.header_area');
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 130) {
			scrollTo.addClass('scroll');
		} else {
			scrollTo.removeClass('scroll');
		}
	});
}

function photoUploader () {
	$('#remove-btn').click(function (e) {
		e.preventDefault();
		$('.edit-image').find('img').attr('src', '');
	});
	$('#upload-btn').click(function (e) {
		e.preventDefault();
		$('.changePhoto').trigger('click');
	});
}

function previewImage () {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("prof-img").files[0]);

	oFReader.onload = function (oFREvent) {
		document.getElementById("prev-img").src = oFREvent.target.result;
	};
}

//======= hamburger button ends=======//


function setScrollBars () {
	$('[data-scrollbar="destinations"]').mCustomScrollbar({
		theme:         'explore-theme',
		scrollEasing:  'linear',
		scrollInertia: 300,
	});
	$('[data-scrollbar="sights"]').mCustomScrollbar({
		theme:         'explore-theme',
		scrollEasing:  'linear',
		scrollInertia: 300,
	});
	$('[data-scrollbar="wine"]').mCustomScrollbar({
		theme:         'explore-theme',
		scrollEasing:  'linear',
		scrollInertia: 300,
	});
}

function addToFav () {
	$('.addToFav').click(function (e) {
		e.preventDefault();
		$(this).find('i').toggleClass('fas');
	});
}
